#!/bin/bash

function askYesNoQuestion
{
    yad \
        --image="$1" \
        --title="$2" \
        --window-icon="$3" \
        --geometry=700x100 \
        --button=No:0 \
        --button=Yes:1 \
        --text-align=center \
        --center \
        --text "$4";
    local code=$?
    if [ "$code" -eq 252 ]; then
        # The user doesn't want to continue here, they pressed the X button
        exit 0
    elif [ "$code" -eq 1 ]; then
        return 1
    # In case there is an unknown exit code, we assume No
    else
        return 0
    fi
}

function askPackageSelectionQuestion
{
    askPackageSelectionQuestion_out=()

    local stdout
    stdout=$(yad \
        --image="$1" \
        --title="$2" \
        --window-icon="$3" \
        --text "$4" \
        --geometry=600x500 \
        --list \
        --checklist \
        --column=Install \
        --column="Package:hd" \
        --column="$5" \
        --center < "$6")

    if [ "$?" -eq 252 ]; then
        # The user doesn't want to continue here, they pressed the X button
        exit 0
    fi

    while IFS= read -r line
    do
        local checked="$(echo "$line" | cut -d '|' -f1)"
        local packages="$(echo "$line" | cut -d '|' -f2)"
        if [ "$checked" == "TRUE" ]; then
            for package in $packages
            do
                askPackageSelectionQuestion_out+=("$package")
            done
        fi
    done < <(printf '%s\n' "$stdout")
}

function isOnline
{
    # Because this can never be offline, right?
    wget -q --spider http://garudalinux.org
    return $?
}

function checkMHWD
{
    local has_nvidia=true
    local could_have_nvidia=true
    mhwd -li --pci | grep -qE "nvidia|optimus" || has_nvidia=false
    if [ "$has_nvidia" == false ]; then
        mhwd -l --pci | grep -E "nvidia|optimus" || could_have_nvidia=false
    fi
    if [ "$has_nvidia" == false ] && [ "$could_have_nvidia" == true ]; then
        if ! askYesNoQuestion "video-display" "Additional drivers" "update" "Additional nonfree NVIDIA graphics drivers are available. Do you want to install them?"; then
            /usr/lib/garuda/launch-terminal "sudo mhwd -a pci nonfree 0300; echo; read -p 'Press enter to finish'"
        fi
    fi
}

function upgrade2ultimate
{
    PACKAGES=()
    # WARNING: Currently not executed in pamac mode!
    PREPARE=()
    SETUP=()
    # Currently disable installation via pamac
    CHECK_PAMAC=false
    HAS_PAMAC_INSTALLER=false

    if [ "$CHECK_PAMAC" = "true" ]; then
        if [ -x "$(command -v pamac-installer)" ]; then
            HAS_PAMAC_INSTALLER=true
        fi
    fi

    local pcChassisTypes=(3 4 6 7 23 24)
    local chassis_type=$(cat /sys/class/dmi/id/chassis_type)
    if [[ " ${pcChassisTypes[*]} " =~ " ${chassis_type} " ]]; then
        if ! askYesNoQuestion "cpu" "Post-installation wizard" "update" "Do you want to apply additional performance tweaks? (at the cost of power usage/heat)"; then
            PACKAGES+=("performance-tweaks")
        fi
    fi

    if ! askYesNoQuestion "printer" "Post-installation wizard" "update" "Do you need Printer, Scanner and Samba Support?"; then
        if pacman -Qs plasma-workspace > /dev/null ; then
            PACKAGES+=("printer-support"
            "scanner-support"
            "samba-support"
            "kdenetwork-filesharing"
            "samba-mounter-git"
            "smb4k"
            "print-manager"
            "skanlite")
        else
            PACKAGES+=("printer-support"
            "scanner-support"
            "samba-support"
            "gvfs-smb"
            "simple-scan")
        fi
    fi

    if ! askYesNoQuestion "wallpaper" "Post-installation wizard" "update" "Do you want to install additional Garuda wallpapers?"; then
        PACKAGES+=("garuda-wallpapers-extra")
    fi

   if pacman -Qs plasma-desktop > /dev/null ; then
        if ! askYesNoQuestion "plasmashell" "Post-installation wizard" "update" "Do you want to install additional KDE components and applications?"; then
            PACKAGES+=("appmenu-gtk-module"
            "ark"
            "bluedevil"
            "breeze"
            "breeze-gtk"
            "colord-kde"
            "dolphin-plugins"
            "drkonqi"
            "filelight"
            "ffmpegthumbs"
            "gwenview"
            "icoutils"
            "kaccounts-providers"
            "kactivitymanagerd"
            "kamera"
            "kamoso"
            "kate"
            "kcalc"
            "kcron"
            "kde-cli-tools"
            "kde-gtk-config"
            "kde-service-menu-reimage"
            "kde-servicemenus-encfs"
            "kde-servicemenus-komparemenu"
            "kde-servicemenus-pdf"
            "kde-servicemenus-pdf-encrypt-decrypt"
            "kde-servicemenus-officeconverter"
            "kde-servicemenus-sendtodesktop"
            "kde-servicemenus-setaswallpaper"
            "kdeconnect"
            "kdecoration"
            "kdegraphics-thumbnailers"
            "kdeplasma-addons"
            "kdf"
            "kdialog"
            "keditbookmarks"
            "kfind"
            "kgamma5"
            "khelpcenter"
            "khotkeys"
            "kimageformats"
            "kinfocenter"
            "kio-extras"
            "kio-fuse"
            "kio-gdrive"
            "kleopatra"
            "kmenuedit"
            "kompare"
            "konsole"
            "krdc"
            "krename"
            "krfb"
            "kscreen"
            "ksshaskpass"
            "ksystemlog"
            "kwalletmanager"
            "kwrited"
            "milou"
            "okular"
            "partitionmanager"
            "plasma-browser-integration"
            "plasma-desktop"
            "plasma-disks"
            "plasma-firewall"
            "plasma-integration"
            "plasma-nm"
            "plasma-pa"
            "plasma-systemmonitor"
            "plasma-thunderbolt"
            "plasma-vault"
            "plasma-workspace"
            "plasma-workspace-wallpapers"
            "polkit-kde-agent"
            "powerdevil"
            "qt5-imageformats"
            "quota-tools"
            "resvg"
            "rootactions-servicemenu"
            "ruby"
            "spectacle"
            "systemsettings"
            "yakuake")
        fi
    fi


   if pacman -Qs gnome-shell > /dev/null ; then
        if ! askYesNoQuestion "gnome-shell" "Post-installation wizard" "update" "Do you want to install additional GNOME applications?"; then
            PACKAGES+=("nautilus-image-converter"
                "nautilus-share"
                "nautilus-sendto"
                "eog-plugins"
                "grilo-plugins"
                "gnome-logs"
                "gnome-sound-recorder"
                "gnome-user-share"
                "lollypop"
                "celluloid")
        fi
    fi


     if ! askYesNoQuestion "aterm" "Post-installation wizard" "update" "Do you need Pentesting software? (installs BlackArch repo + settings)"; then
         PACKAGES+=("garuda-blackarch"
                     "blackarch-keyring"
                     "blackarch-menus"
                     "blackarch-mirrorlist")
     fi

     # Absolute HACK!
     if [[ "${PACKAGES[@]}" =~ "garuda-blackarch" ]]; then
         PREPARE+=("sh <(wget -qO- https://blackarch.org/strap.sh)")
     fi

     # Absolute HACK!
     if [[ "${PACKAGES[@]}" =~ "garuda-blackarch" ]]; then
         SETUP+=("sed -i 's/#server/server/g' /etc/pacman.d/blackarch-mirrorlist")
     fi

    askPackageSelectionQuestion 'fcitx' "Post-installation wizard" 'update' "What additional input method support do you want?" "Applications" "/usr/lib/setup-assistant/input-method.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'discover' "Post-installation wizard" 'update' "What extra Software centers do you want?" "Applications" "/usr/lib/setup-assistant/pkgmngrs.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'aterm' "Post-installation wizard" 'update' "Do you need another kernel?" "Applications" "/usr/lib/setup-assistant/kernels.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'libreoffice-main' "Post-installation wizard" 'update' "What Office suites do you want?" "Office" "/usr/lib/setup-assistant/office.txt"

    # Absolute HACK!
    if [[ "${askPackageSelectionQuestion_out[@]}" =~ "libreoffice-fresh" ]] && [[ "${askPackageSelectionQuestion_out[@]}" =~ "libreoffice-still" ]] ; then
        askPackageSelectionQuestion_out=( "${askPackageSelectionQuestion_out[@]/libreoffice-still}" )
    fi

    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'firefox' "Post-installation wizard" 'update' "What extra browsers do you want?" "Applications" "/usr/lib/setup-assistant/browsers.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'thunderbird' "Post-installation wizard" 'update' "What email clients do you want?" "Applications" "/usr/lib/setup-assistant/mail.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'telegram-desktop' "Post-installation wizard" 'update' "What communication software do you want?" "Applications" "/usr/lib/setup-assistant/communication.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'kget' "Post-installation wizard" 'update' "What common internet software do you want?" "Applications" "/usr/lib/setup-assistant/internet.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'audacity' "Post-installation wizard" 'update' "What audio software do you want?" "Applications" "/usr/lib/setup-assistant/audio.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'kdenlive' "Post-installation wizard" 'update' "What video software do you want?" "Applications" "/usr/lib/setup-assistant/video.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'gimp' "Post-installation wizard" 'update' "What graphics software do you want?" "Applications" "/usr/lib/setup-assistant/graphics.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'gimp' "Post-installation wizard" 'update' "What multimedia software do you want?" "Applications" "/usr/lib/setup-assistant/multimedia.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    askPackageSelectionQuestion 'code' "Post-installation wizard" 'update' "What development software do you want?" "Applications" "/usr/lib/setup-assistant/development.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    # Absolute HACK!
    if [[ "${askPackageSelectionQuestion_out[@]}" =~ "podman" ]]; then
        SETUP+=("systemctl enable --now podman.socket")
    fi

    # Absolute HACK!
    if [[ "${askPackageSelectionQuestion_out[@]}" =~ "docker" ]]; then
        SETUP+=("systemctl enable --now docker.socket")
    fi

    askPackageSelectionQuestion 'virtualbox' "Post-installation wizard" 'update' "What virtualization software do you want?" "Applications" "/usr/lib/setup-assistant/virtualization.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    # Absolute HACK!
    if [[ "${askPackageSelectionQuestion_out[@]}" =~ "virt-manager-meta" ]] || [[ "${askPackageSelectionQuestion_out[@]}" =~ "gnome-boxes" ]]; then
        SETUP+=("systemctl enable --now libvirtd")
    fi

    askPackageSelectionQuestion 'anbox' "Post-installation wizard" 'update' "What other software do you want?" "Applications" "/usr/lib/setup-assistant/other.txt"
    PACKAGES=("${PACKAGES[@]}" "${askPackageSelectionQuestion_out[@]}")

    if [ "$HAS_PAMAC_INSTALLER" = "true" ]; then
        if ! pamac-installer ${PACKAGES[@]} ; then
            exit 0
        fi
        pkexec bash -c "$( IFS=$'\n'; echo "${SETUP[*]}" )"
    else
        prepare_file="$(mktemp)"
        packages_file="$(mktemp)"
        setup_file="$(mktemp)"
        printf '%s\n' "${PACKAGES[@]}" > "$packages_file"
        echo "$( IFS=$'\n'; echo "${SETUP[*]}" )" > "$setup_file"
        echo "$( IFS=$'\n'; echo "${PREPARE[*]}" )" > "$prepare_file"
        /usr/lib/garuda/launch-terminal "/usr/lib/setup-assistant/apply.sh \"$prepare_file\" \"$packages_file\" \"$setup_file\""
        rm "$packages_file" "$setup_file" "$prepare_file"
    fi
}

if [ -z "$SETUP_ASSISTANT_SELFUPDATE" ]; then
    while ! isOnline
    do
        if askYesNoQuestion "update" "Setup Assistant" "update" "No internet connection available, try again?"; then
            exit 0
        fi
    done

    if ! askYesNoQuestion "update" "Setup Assistant" "update" "Update the system? (recommended!)"; then
        /usr/lib/garuda/launch-terminal "update remote setup; read -p 'Press enter to continue'"
        # Allow the setup assistant to self update
        SETUP_ASSISTANT_SELFUPDATE=1 exec setup-assistant
    fi
else
    checkMHWD
fi

if ! askYesNoQuestion "update" "Setup Assistant" "update" "Start the post installation wizard (Select your favorites from a lot of commonly used packages)"; then
    upgrade2ultimate
fi

if command -v pamac-installer &> /dev/null && ! askYesNoQuestion "update" "Setup Assistant" "update" "Do you want to remove the setup assistant?"; then
        pamac-installer --remove garuda-setup-assistant
fi
